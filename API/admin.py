from django.contrib import admin

from .models import City, Measurement, Weather

class WeatherAdmin(admin.ModelAdmin):
    list_display = ('type', 'datetime', 'measurement')

    # def __self__(self):
    #     return 'Weather for {}'.format(self.measurement)

admin.site.register(City)
admin.site.register(Measurement)
admin.site.register(Weather, WeatherAdmin)