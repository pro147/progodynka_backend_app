from django.urls import include, path
from django.contrib import admin
from rest_framework import routers
from django.urls import path

from .views import CityViewSet, MeasurementViewSet, AlertViewSet

router = routers.DefaultRouter()
router.register('city', CityViewSet)
router.register('measurement', MeasurementViewSet)
router.register('alert', AlertViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
]
