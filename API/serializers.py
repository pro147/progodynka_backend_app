from rest_framework.serializers import ModelSerializer

from .models import City, Measurement, Weather, Alert

class CitySerializer(ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'

class WeatherSerializer(ModelSerializer):
    class Meta:
        model = Weather
        fields = '__all__'

class MeasurementSerializer(ModelSerializer):
    weather = WeatherSerializer(many=True, read_only=True)
    class Meta:
        model = Measurement
        fields = ['pk', 'datetime','city','alert','weather']

class AlertSerializer(ModelSerializer):
    class Meta:
        model = Alert
        fields = '__all__'
