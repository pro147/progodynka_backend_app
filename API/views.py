from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from django.db import transaction
from django.db.models import Q
from rest_framework.exceptions import ParseError

from .models import City, Measurement, Weather, Alert
from .serializers import CitySerializer, MeasurementSerializer, AlertSerializer


from datetime import datetime, timedelta

import requests
from pprint import pprint as pp

class CityViewSet(ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    #permission_classes = [AllowAny]

    @transaction.atomic
    def geocoding(request, name):
        key = '478a114b3200f76271d17d85ec3e972d'
        url = 'http://api.openweathermap.org/geo/1.0/direct?q={}&limit=5&appid={}'.format(name, key)

        res = requests.get(url).json()

        queryset = City.objects.all().filter(
            Q(countryCode=res[0]['country']),
            Q(name=res[0]['name']),
            Q(state=res[0]['state']) | Q(state=None)
        )

        if queryset.count() == 0:
            city = City(
                name=res[0]['name'],
                lat=res[0]['lat'],
                lon=res[0]['lon'],
                countryCode=res[0]['country'],
            )

            try:
                city.state = res[0]['state']
            except:
                city.state = None

            city.save()

            return City.objects.all().filter(pk=city.pk)

        return queryset

    @transaction.atomic
    def reverseGeocoding(request, lat, lon):
        key = '478a114b3200f76271d17d85ec3e972d'
        url = 'http://api.openweathermap.org/geo/1.0/reverse?lat={}&lon={}&limit=10&appid={}'.format(lat, lon, key)

        res = requests.get(url).json()

        queryset = City.objects.all().filter(
            Q(countryCode=res[0]['country']),
            Q(name=res[0]['name']),
            Q(state=res[0]['state']) | Q(state=None)
        )

        if queryset.count() == 0:
            city = City(
                name=res[0]['name'],
                lat=res[0]['lat'],
                lon=res[0]['lon'],
                countryCode=res[0]['country'],
            )

            try:
                city.state = res[0]['state']
            except:
                city.state = None

            city.save()

            return City.objects.all().filter(pk=city.pk)

        return queryset

    def get_queryset(self):
        queryset = City.objects.all()
        name = self.request.query_params.get('name')
        lat = self.request.query_params.get('lat')
        lon = self.request.query_params.get('lon')
        force = self.request.query_params.get('force')

        if name:
            queryset = queryset.filter(name__contains=name)

            if queryset.count() == 0 or bool(force) == True:
                return self.geocoding(name)

        elif lat and lon:
            return self.reverseGeocoding(lat, lon)

        return queryset


class MeasurementViewSet(ModelViewSet):
    queryset = Measurement.objects.all()
    serializer_class = MeasurementSerializer
    #permission_classes = [IsAuthenticated]

    @transaction.atomic
    def createNewMeasurement(request, cityID):
        city = City.objects.get(pk=cityID)
        key = '478a114b3200f76271d17d85ec3e972d'

        url = 'https://api.openweathermap.org/data/2.5/onecall?appid={}&lat={}&lon={}&exclude=minutely&units=metric'.format(key, city.lat, city.lon)
        measurement = Measurement(city=city)
        measurement.save()

        res = requests.get(url).json()

        # pp.pprint(res)

        # Current weather
        weather_current = Weather(
            type = Weather.WeatherTypeChoices.current,
            datetime = datetime.fromtimestamp(res['current']['dt']),
            sunrise = datetime.fromtimestamp(res['current']['sunrise']),
            sunset = datetime.fromtimestamp(res['current']['sunset']),
            temperature = res['current']['temp'],
            maxTemperature = res['daily'][0]['temp']['max'],
            minTemperature = res['daily'][0]['temp']['min'],
            temperatureFeelsLike = res['current']['feels_like'],
            pressure = res['current']['pressure'],
            humidity = res['current']['humidity'],
            uvi = res['current']['uvi'],
            clouds = res['current']['clouds'],
            visibility = res['current']['visibility'],
            windSpeed = res['current']['wind_speed'],
            windDegree = res['current']['wind_deg'],
            weatherID = res['current']['weather'][0]['id'],
            propability = None,
            measurement = measurement
        )

        try:
            weather_current.rain = res['current']['rain']['1h']
        except:
            weather_current.rain = 0
        try:
            weather_current.snow = res['current']['snow']['1h']
        except:
            weather_current.snow = 0

        weather_current.save()

        # Daily forecast
        for id, data in enumerate(res['daily']):
            if id != 0:
                daily_weather = Weather(
                    type = Weather.WeatherTypeChoices.forecast_daily,
                    datetime = datetime.fromtimestamp(data['dt']),
                    sunrise = datetime.fromtimestamp(data['sunrise']),
                    sunset = datetime.fromtimestamp(data['sunset']),
                    temperature = 0,
                    maxTemperature = data['temp']['max'],
                    minTemperature = data['temp']['min'],
                    temperatureFeelsLike = data['feels_like']['day'],
                    pressure = data['pressure'],
                    humidity = data['humidity'],
                    uvi = data['uvi'],
                    clouds = data['clouds'],
                    visibility = 0,
                    windSpeed = data['wind_speed'],
                    windDegree = data['wind_deg'],
                    weatherID = data['weather'][0]['id'],
                    propability = data['pop'],
                    measurement = measurement
                )

                try:
                    daily_weather.rain = data['rain']
                except:
                    daily_weather.rain = 0

                try:
                    daily_weather.snow = res['snow']
                except:
                    daily_weather.snow = 0

                daily_weather.save()


        # Hourly forecast
        for id, data in enumerate(res['hourly']):

            if id > 12: break

            hourly_weather = Weather(
                type = Weather.WeatherTypeChoices.forecast_hourly,
                datetime = datetime.fromtimestamp(data['dt']),
                temperature = data['temp'],
                temperatureFeelsLike = data['feels_like'],
                pressure = data['pressure'],
                humidity = data['humidity'],
                uvi = data['uvi'],
                clouds = data['clouds'],
                visibility = data['visibility'],
                windSpeed = data['wind_speed'],
                windDegree = data['wind_deg'],
                weatherID = data['weather'][0]['id'],
                propability = data['pop'],
                measurement = measurement
            )

            try:
                hourly_weather.rain = data['rain']['1h']
            except:
                hourly_weather.rain = 0

            try:
                hourly_weather.snow = res['snow']['1h']
            except:
                hourly_weather.snow = 0

            hourly_weather.save()

        return Measurement.objects.all().filter(pk=measurement.pk)


    def get_queryset(self):
        queryset = Measurement.objects.all()
        city = self.request.query_params.get('city')

        if city:
            now = datetime.now() - timedelta(minutes=15)
            queryset = queryset.filter(city=city, datetime__gte=now)

            if queryset.count() == 0:
                print(city)
                return self.createNewMeasurement(cityID=city)

        else:
            raise ParseError('City not specified')

        return queryset

class AlertViewSet(ModelViewSet):
    queryset = Alert.objects.all()
    serializer_class = AlertSerializer
    permission_classes = [IsAuthenticated]

