# Generated by Django 4.0.3 on 2022-04-29 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0002_alter_weather_measurement'),
    ]

    operations = [
        migrations.AlterField(
            model_name='weather',
            name='type',
            field=models.CharField(choices=[('current', 'Current'), ('forecast_daily', 'Forecast Daily'), ('forecast_hourly', 'Forecast Hourly'), ('historical', 'Historical')], max_length=15),
        ),
    ]
