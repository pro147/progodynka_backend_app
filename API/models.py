from django.db import models

class City(models.Model):
    name = models.TextField()
    lat = models.FloatField()
    lon = models.FloatField()
    countryCode = models.CharField(max_length=10)
    state = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )

class Alert(models.Model):
    sender = models.TextField()
    name = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    description = models.TextField()

class Measurement(models.Model):
    datetime = models.DateTimeField(
        auto_now_add=True,
        blank=True
    )
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
    )
    alert = models.ForeignKey(
        Alert,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

class Weather(models.Model):
    class WeatherTypeChoices(models.TextChoices):
        current = 'current'
        forecast_daily = 'forecast_daily'
        forecast_hourly = 'forecast_hourly'
        historical = 'historical'

    type = models.CharField(
        max_length=15,
        choices=WeatherTypeChoices.choices
    )


    datetime = models.DateTimeField()
    sunrise = models.DateTimeField(blank=True, null=True) # none for hourly
    sunset = models.DateTimeField(blank=True, null=True) # none for hourly
    temperature = models.FloatField()
    maxTemperature = models.FloatField(blank=True, null=True) # none for current, hourly
    minTemperature = models.FloatField(blank=True, null=True) # none for current, hourly
    temperatureFeelsLike = models.FloatField()
    pressure = models.FloatField()
    humidity = models.FloatField()
    uvi = models.FloatField()
    clouds = models.FloatField()
    visibility = models.FloatField()
    windSpeed = models.FloatField()
    windDegree = models.FloatField()
    rain = models.FloatField()
    snow = models.FloatField()
    weatherID = models.IntegerField()
    propability = models.FloatField(blank=True, null=True) # none for current
    measurement = models.ForeignKey(
        Measurement,
        related_name="weather",
        on_delete=models.CASCADE,
    )